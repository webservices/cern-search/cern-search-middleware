#!/usr/bin/python

import os

from cern_search_middleware.utils import evaluate_config_param


def create_filter(attributes):
    attributes = evaluate_config_param(attributes)
    qfilter = ''
    for attr, value in attributes.iteritems():
        qfilter = '{current_query}+AND+{attr}:{value}'.format(
            current_query=qfilter,
            attr=attr,
            value=value
        )

    return qfilter[5:]


query_filter = create_filter(os.getenv('SEARCH_MIDDLEWARE_QUERY_FILTER'))
search_instance = os.getenv('SEARCH_MIDDLEWARE_INSTANCE')
search_instance_token = os.getenv('SEARCH_MIDDLEWARE_INSTANCE_TOKEN')
