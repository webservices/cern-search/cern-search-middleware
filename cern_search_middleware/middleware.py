#!/usr/bin/python

import urllib

import requests
from flask import Flask, request, make_response
from flask_restful import Api, Resource
from cern_search_middleware import config

app = Flask(__name__)
api = Api(app)


class Middleware(Resource):
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": 'Bearer {credentials}'.format(credentials=config.search_instance_token)
    }

    def get(self):
        query = request.values.get('q', '', type=str)
        query = urllib.quote(query)
        api_query = '{instance}/api/records/?q={query_filter}'.format(
            instance=config.search_instance,
            query_filter=config.query_filter)

        if query:
            api_query = '{api_query}+AND+content:{query}'.format(
                api_query=api_query,
                query=query
            )

        requests_response = requests.get(api_query, headers=self.headers)

        return make_response(requests_response.text, requests_response.status_code)


api.add_resource(Middleware, '/docs', endpoint='docs')
