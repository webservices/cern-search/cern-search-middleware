#!/usr/bin/python

import ast


def evaluate_config_param(param):
    # Evaluate value
    try:
        return ast.literal_eval(param)

    except (SyntaxError, ValueError):
        return None
