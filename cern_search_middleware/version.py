#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Version information for CERN Search Middleware.
This file is imported by ``cern_search_middleware.__init__``,
and parsed by ``setup.py``.
"""

from __future__ import absolute_import, print_function

__version__ = '0.0.1'
