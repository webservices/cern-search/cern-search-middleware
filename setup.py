#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

from setuptools import find_packages, setup

readme = open('README.md').read()
install_requires = [
    'flask>=1.0.2,<1.1.0',
    'flask_restful>=0.3.6,<0.4.0',
    'requests>=2.19.1,<2.20.0',
    'uwsgi>=2.0.17.1,<2.0.18.0',
]


packages = find_packages()

# Get the version string. Cannot be done with import!
g = {}
with open(os.path.join("cern_search_middleware", "version.py"), "rt") as fp:
    exec(fp.read(), g)
    version = g["__version__"]

setup(
    name='cern-search-middleware',
    version=version,
    description='CERN Search Middleware',
    long_description=readme,
    license='GPLv3',
    author='CERN',
    author_email='cernsearch.support@cern.ch',
    url='http://search.cern.ch/',
    packages=packages,
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=install_requires,
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Development Status :: 1 - Pre-Alpha',
    ],
)
