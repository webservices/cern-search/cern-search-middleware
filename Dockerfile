# -*- coding: utf-8 -*-

# Use CentOS7:
FROM cern/cc7-base

# Install pre-requisites
RUN yum update -y && \
    yum install -y \
    	gcc \
    	python-devel \
        python-pip && \
    pip install --upgrade pip

ADD requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt

ADD . /middleware
WORKDIR /middleware

EXPOSE 5000
EXPOSE 8000

# uWSGI configuration
ARG UWSGI_WSGI_MODULE=cern_search_middleware.middleware:app
ENV UWSGI_WSGI_MODULE ${UWSGI_WSGI_MODULE:-cern_search_middleware.middleware:app}
ARG UWSGI_PORT=5000
ENV UWSGI_PORT ${UWSGI_PORT:-5000}
ARG UWSGI_PROCESSES=2
ENV UWSGI_PROCESSES ${UWSGI_PROCESSES:-2}
ARG UWSGI_THREADS=2
ENV UWSGI_THREADS ${UWSGI_THREADS:-2}

CMD uwsgi --module ${UWSGI_WSGI_MODULE} --http 0.0.0.0:${UWSGI_PORT} --master --processes ${UWSGI_PROCESSES} --threads ${UWSGI_THREADS} --stats 0.0.0.0:8000

